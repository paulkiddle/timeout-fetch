# Timeout Fetch

Creates an instance of fetch that aborts after a certain time period.

```javascript
import timeoutFetch, { TimeoutError } from 'timeout-fetch';
import nodeFetch from 'node-fetch';

// Timeout is defined in miliseconds
const ONE_SECOND = 1000;
const fetch = timeoutFetch(
	ONE_SECOND,
	nodeFetch // Defaults to globalThis.fetch if you don't provide a fetch implementation yourself
);

try {
	await fetch('http://example.com');
	console.log('Fetch success');
} catch(e) {
	if(e instanceof TimeoutError) {
		console.log('Fetch timed out');
	} else {
		console.log('Another error occured');
	}
}
```

## Dependencies
None
<a name="module_timeout-fetch"></a>

## timeout-fetch
Timeout Fetch

**See**: [module:timeout-fetch.default](module:timeout-fetch.default)  

* [timeout-fetch](#module_timeout-fetch)
    * [module.exports(count, fetch)](#exp_module_timeout-fetch--module.exports) ⇒ <code>function</code> ⏏
        * [.TimeoutError](#module_timeout-fetch--module.exports.TimeoutError)

<a name="exp_module_timeout-fetch--module.exports"></a>

### module.exports(count, fetch) ⇒ <code>function</code> ⏏
Creates a fetch function that is aborted after a timeout period

**Kind**: Exported function  
**Returns**: <code>function</code> - The created fetch function. Has the same signature as the passed param.  

| Param | Type | Description |
| --- | --- | --- |
| count | <code>number</code> | The timeout duration in miliseconds |
| fetch | <code>function</code> | The fetch function to call, defaults to the global fetch |

<a name="module_timeout-fetch--module.exports.TimeoutError"></a>

#### module.exports.TimeoutError
Error that is thrown when the timeout expires

**Kind**: static class of [<code>module.exports</code>](#exp_module_timeout-fetch--module.exports)  

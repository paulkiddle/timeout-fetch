import { expect, jest, test } from '@jest/globals';
import TimeoutFetch, { timeout, TimeoutError } from './index.js';

const sleep = ms => new Promise(r=>setTimeout(r, ms));

test('timeout throws on abort', async ()=>{
	const it = timeout(100);

	const signal = it.next().value;

	expect(signal.aborted).toBe(false);

	await sleep(100);

	expect(signal.aborted).toBe(true);

	expect(()=>it.throw(new Error('Abort signal'))).toThrow(TimeoutError);
});

test('timeout returns value and clears timeout on success', async ()=>{
	const it = timeout(100);

	const signal = it.next().value;

	expect(signal.aborted).toBe(false);

	expect(it.return('rtn').value).toEqual('rtn');

	await sleep(100);

	expect(signal.aborted).toBe(false);
});

test('timeout throws error if error occurs before timer expires', async ()=>{
	const it = timeout(100);

	const signal = it.next().value;

	expect(signal.aborted).toBe(false);

	class MiscError extends Error {}

	expect(()=>it.throw(new MiscError)).toThrow(MiscError);

	await sleep(100);

	expect(signal.aborted).toBe(false);
});

test('timeout fetch calls fetch function', async ()=>{
	const fetch = globalThis.fetch = jest.fn(()=>'rtn');

	const timeoutFetch = TimeoutFetch(10);

	const req = 'req';

	expect(await timeoutFetch(req)).toBe('rtn');

	expect(fetch).toHaveBeenCalledWith(req, { signal: expect.any(AbortSignal) });
});